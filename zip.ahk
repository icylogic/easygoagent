Zip(FilesToZip,sZip)
{
    If Not FileExist(sZip)
        CreateZipFile(sZip)
    psh := ComObjCreate( "Shell.Application" )
    pzip := psh.Namespace( sZip )
    if InStr(FileExist(FilesToZip), "D")
        FilesToZip .= SubStr(FilesToZip,0)="\" ? "*.*" : "\*.*"
    loop,%FilesToZip%,1
    {
        zipped++
        ToolTip Zipping %A_LoopFileName% ..
        pzip.CopyHere( A_LoopFileLongPath, 4|16 )
        Loop
        {
            done := pzip.items().count
            if done = %zipped%
                break
        }
        done := -1
    }
    ToolTip
}

CreateZipFile(sZip)
{
    Header1 := "PK" . Chr(5) . Chr(6)
    VarSetCapacity(Header2, 18, 0)
    file := FileOpen(sZip,"w")
    file.Write(Header1)
    file.RawWrite(Header2,18)
    file.close()
}

Unz(sZip, sUnz)
{
    fso := ComObjCreate("Scripting.FileSystemObject")
    If Not fso.FolderExists(sUnz)
       FileCreateDir, %sUnz% 
    psh  := ComObjCreate("Shell.Application")
    zippedItems := psh.Namespace( sZip ).items().count
    psh.Namespace( sUnz ).CopyHere( psh.Namespace( sZip ).items, 4|16 )
    Loop {
        sleep 50
        unzippedItems := psh.Namespace( sUnz ).items().count
        IfEqual,zippedItems,%unzippedItems%
            break
    }
}
