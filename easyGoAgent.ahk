﻿#include isConnected.ahk
#include zip.ahk
#include log.ahk

checkNetWork()

global GoAgentFolder := getGoAgentFolder()
global appid := getAppid()

verSelection := getVerSelection()
downloadByVer(ver)

unzipPackage()
backup()
upgrade()

upload()
setStartup()

MsgBox, 4,, 现在启动GoAgent?, 3
IfMsgBox Yes
Run, %GoAgentFolder%\local\goagent.exe

ExitApp

;functions
checkNetWork()
{
inilog(A_ScriptName, "start checkNetWork()")
    if (!isConnected())
    {
        MsgBox, 4,, 检测到网络未连接`n点击确定`, 此程序将隐藏至后台`, 当连接至网络时自动恢复运行`n点击取消`, 此程序将退出, 5
        IfMsgBox No 
        {
            ExitApp
        } 
        else
        {
            while (!isConnected())
            {
                sleep, 5000
            }
            Msgbox,,,检测到网络已连接`, 恢复运行, 2
        }
    }
    inilog(A_ScriptName, "end checkNetWork()")
}

getGoAgentFolder()
{
    inilog(A_ScriptName, "start getGoAgentFolder()")
    IniRead, GoAgentFolder, settings.ini, user info, GoAgentFolder, %A_Space%
    Loop
    {
        if (GoAgentFolder != "")
            break
        FileSelectFolder, GoAgentFolder, *StartingFolder, 3, 选择要安装GoAgent的文件夹, 如果已安装过GoAgent, 请选择local这个文件夹的上一层目录
    }
    IniWrite, %GoAgentFolder%, settings.ini, user info, GoAgentFolder
    return GoAgentFolder
    inilog(A_ScriptName, "end getGoAgentFolder()")
}

getAppid()
{
    IniRead, appid, settings.ini, user info, appid, %A_Space%
    IniRead, appid, %GoAgentFolder%\local\proxy.ini, gae, appid, %A_Space%
    Loop
    {
        if (appid != "")
            break
        InputBox, appid, , 输入你的appid "appid1|appid2" `n`n如果你还没有一个appid`n`n请去 https://appengine.google.com/ 申请一个
    }
    IniWrite, %appid%, settings.ini, user info, appid
    return appid
    inilog(A_ScriptName, "end getAppid()")
}

getVerSelection()
{
    IniRead, verSelection, settings.ini, user info, verSelection, %A_Space%
    if (verSelection = "")
    {
        ver := firstSelectVer()
        IniWrite, %ver%, settings.ini, user info, verSelection
        Return ver
    }
    else
    {
        Return verSelection
    }
    inilog(A_ScriptName, "end getVerSelection()")
}

firstSelectVer()
{
    #SingleInstance
    SetTimer, ChangeButtonNames, 1
    MsgBox, 4, Version Select, 点击 是 选择稳定版`n点击 否 选择最新版`n(稳定版即GoogleCode最新发布版本, 最新版即github上最新一次commit, 建议选稳定版)
    IfMsgBox, YES
        Return "stable"
    else 
        Return "latest"

    ChangeButtonNames:
    IfWinExist, Version Select
    {
        SetTimer, ChangeButtonNames, off 
        WinActivate
        ControlSetText, Button1, &stable
        ControlSetText, Button2, &latest
        Return
    }
    else
    {
        Return
    }
    inilog(A_ScriptName, "end firstSelectVer()")
}

downloadByVer(ver)
{
    inilog(A_ScriptName, "start downloadByVer()")

    UrlDownloadToFile, https://github.com/goagent/goagent/releases, release.html
    UrlDownloadToFile, https://raw.github.com/goagent/goagent/3.0/local/proxy.py, proxy.py

    Loop, read, release.html
    {
        URLSearchString = %A_LoopReadLine%
            ;get latestDownloadLink
        if ( InStr(URLSearchString, "/goagent/goagent/archive/") And !InStr(URLSearchString, "nofollow") )
        {
            StringGetPos, lp, URLSearchString, /goagent/goagent/archive/
            StringTrimLeft, hrefText, URLSearchString, lp
            StringGetPos, rp, hrefText, zip
            StringLeft, hrefText, hrefText, rp+3
            latestDownloadLink = https://github.com%hrefText%
        }
    ;get stableDownloadLink & stable version
        if ( InStr(URLSearchString, "/goagent/goagent/archive/") And InStr(URLSearchString, "nofollow") )
        {
            StringGetPos      , lp, URLSearchString, /goagent/goagent/archive/
            StringTrimLeft    , hrefText, URLSearchString, lp
            StringGetPos      , rp, hrefText, zip
            StringLeft        , hrefText, hrefText, rp+3
            stableDownloadLink = https://github.com%hrefText%

            StringGetPos  , fileNamePos, hrefText, /, R
            StringTrimLeft, fileName, hrefText, fileNamePos+1
            StringGetPos  , versionPos, fileName, zip
            StringLeft    , stableVersion, fileName, versionPos-1
            vPos := InStr(stableVersion, v)
            StringTrimLeft, stableVersion, stableVersion, vPos
            Break
        }
    }

    contributors=

    Loop, read, proxy.py
    {
        versionSearchString = %A_LoopReadLine%

        if (InStr(versionSearchString, "@"))
        {
            contributors = %contributors%`n%versionSearchString%
        }
    ;get latest version
        if (InStr(versionSearchString, "__version__ ="))
        {
            StringGetPos      , lp, versionSearchString, '
            StringTrimLeft    , versionText, versionSearchString, lp+1
            StringTrimRight   , latestVersion, versionText, 1
            Break
        }
    }

    FileDelete, release.html
    FileDelete, proxy.py

    localVersion := getLocalVersion()
    if (ver = "stable")
    {
        if (localVersion != stableVersion)
        {
            UrlDownloadToFile, %stableDownloadLink%, temp.zip
            Msgbox ,,, 已下载稳定版 %stableVersion%, 3
        }
        else
        {
            Msgbox ,,, 当前稳定版无更新, 3
            ExitApp
        }
    }
    else
    {
        UrlDownloadToFile, %latestDownloadLink%, temp.zip
        Msgbox ,,, 已下载最新版 %latestVersion%, 3
    }

    Msgbox ,,,感谢: `n%contributors%`n, 3
    inilog(A_ScriptName, "end downloadByVer()")
}

getLocalVersion()
{
    Loop, read, d:\portable\GoAgent\local\proxy.py
    {
        versionSearchString = %A_LoopReadLine%
        if (InStr(versionSearchString, "__version__ ="))
        {
            StringGetPos      , lp, versionSearchString, '
            StringTrimLeft    , versionText, versionSearchString, lp+1
            StringTrimRight   , localVersion, versionText, 1
            Break
        }
    }
    return %localVersion%
}

unzipPackage()
{
    workingFolder = %A_WorkingDir%\temp
    downloadFile = %A_WorkingDir%\temp.zip
    Msgbox ,,,正在解压缩,2
    Unz(downloadFile, workingFolder)
    Msgbox ,,,解压缩完成,2
    FileDelete, temp.zip
}

backup()
{
    Msgbox ,,,正在备份本地GoAgent, 2
    backupFile = %A_WorkingDir%\GoAbackup-(%A_YEAR%-%A_Mon%-%A_DD%_%A_Hour%h%A_Min%m%A_Sec%s).zip
    Zip(GoAgentFolder, backupFile)
    MsgBox ,,,请检查你的本地备份文件,3
    MsgBox, 48,,
    (
     WARNING: 注意 最新版本有可能不能正常工作 
     如果你发现异常, 请删除GoAgent目录, 使用 %A_WorkingDir%\GoAbackup-(%A_YEAR%-%A_Mon%-%A_DD%_%A_Hour%h%A_Min%m%A_Sec%s).zip 中的文件来恢复. 此时, 如果你希望恢复后再次尝试更新, 请删除 %A_WorkingDir% 下的 settings.ini 并重启此程序
    )
    Run, %A_WorkingDir%
}

upgrade()
{
    MsgBox, 如果 GoAgent 正在运行, 请手动退出该程序, 然后点击 确定 来完成更新

    FileCopyDir, temp\goagent-3.0, %GoAgentFolder%, 1
    IniWrite, %A_Space%%appid%, %GoAgentFolder%\local\proxy.ini, gae, appid%A_Space%
    FileRemoveDir, temp, 1
}

upload()
{
    MsgBox, 4,, 是否需要重新上传? `n`n(如果你不确定, 请不要关闭此窗口, 然后前往`n`nhttps://code.google.com/p/goagent/ 查阅当前版本是否需要重新上传)
    IfMsgBox Yes
    RunWait, %GoAgentFolder%\server\uploader.bat
}

setStartup()
{
    MsgBox, 4,, 更新已完成 `n`n是否开机启动此程序以自动更新 GoAgent?
    IfMsgBox Yes
    FileCreateShortcut, %A_WorkingDir%\easyGoAgent.exe, %A_Startup%\easyGoAgent.lnk, %A_WorkingDir%

    MsgBox, 4,, 是否开机启动 GoAgent?
    IfMsgBox Yes
    FileCreateShortcut, %GoAgentFolder%\local\goagent.exe, %A_Startup%\goagent.lnk
}
