用于自动更新GoAgent (可选择最新版或稳定版, 即GoAgent[github项目](https://github.com/goagent/goagent)的最新[release](https://github.com/goagent/goagent/releases)或google code主页[发行版](https://code.google.com/p/goagent/)) 并且保存用户信息, 只要此版本不需要重新上传, 即可无需配置马上使用.

也可以用于初次配置, 只需在[Google app engine](https://appengine.google.com/)申请APPID后按提示操作即可.

已存在的Proxy.ini中appid优先级高于settings.ini